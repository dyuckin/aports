# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=listenbrainz-mpd
pkgver=2.3.4
pkgrel=0
pkgdesc="ListenBrainz submission client for MPD written in Rust"
url="https://codeberg.org/elomatreb/listenbrainz-mpd"
arch="all"
license="AGPL-3.0-only"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	sqlite-dev
"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
"
source="$pkgname-$pkgver.tar.gz::https://codeberg.org/elomatreb/listenbrainz-mpd/archive/v$pkgver.tar.gz"
builddir="$srcdir/$pkgname"
options="net !check" # no tests

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --all-features
}

package() {
	install -Dm755 target/release/listenbrainz-mpd "$pkgdir"/usr/bin/listenbrainz-mpd

	install -Dm644 listenbrainz-mpd.adoc -t "$pkgdir"/usr/share/doc/$pkgname/
	install -Dm644 LICENSE.txt "$pkgdir"/usr/share/licenses/$pkgname/LICENSE

	install -Dm644 generated_completions/listenbrainz-mpd.bash "$pkgdir"/usr/share/bash-completion/completions/listenbrainz-mpd
	install -Dm644 generated_completions/listenbrainz-mpd.fish "$pkgdir"/usr/share/fish/vendor_completions.d/listenbrainz-mpd.fish
	install -Dm644 generated_completions/_listenbrainz-mpd "$pkgdir"/usr/share/zsh/site-functions/_listenbrainz-mpd
}

sha512sums="
d2523b9194e992fa6c50e9819adbb462e9f5b2921ef1cc9b2680a3dcd38f72e80f5eab123363f33ebd7ff044cf6519bbee9ff270d1bdbc8d64046005d7431b8d  listenbrainz-mpd-2.3.4.tar.gz
"
